define(['lib','auth','app/header','components/tabPage','app/sidebar','components/dialog','text!app/app.tpl'], function(LIB,a,h,p,s,d,tpl) {
	AppView = Backbone.View.extend({
		el: $("#content"),
		template: _.template(tpl),
		initialize: function() {
			
			//构造函数，实例化一个World集合类
			//并且以字典方式传入AppView的对象
			this.render();			
	
			this.sidebar =  new SidebarView({el:$('.main-sidebar')});
			this.header =  new HeaderView({el:$('.main-header')});
			this.tabPage= new TabPageView({el:$('#body-content')});
			this.d = new DialogView({el:$('#_temp')});
			Backbone.on(LIB.getConst().MESSAGE.ADD_TAB_PAGE, this.addTabPage, this);//监听消息			
		},
		render: function() {
            this.$el.html(this.template({who: '111111World!1111'}));
      	},
		events: {
			//事件绑定，绑定Dom中id为check的元素
			"click #popWin":"popWin",
			"click #check": "checkIn",
			"click #check2": "checkIn2",
		},
		popWin:function(){
			this.d.openDialog('请登录','page/login.html');
		},
		checkIn: function() {			
			this.tabPage.addTab('新窗口','page/login.html');
		},
		checkIn2: function() {			
			this.tabPage.addTab('新窗口222','page/table.html');
		},
		printf:function(e){
			console.log("printf....." + e.tt);
		},
		addTabPage:function(e){
			this.tabPage.addTab(e.title,e.url,e.icon);
		}
	});
	//实例化AppView
	var appview = new AppView;	
});