<li class="header">主菜单导航</li>
<%$.each(datas, function (i,item) {%>
	<li class="treeview" id="<%=item.id%>" act="<%=item.url%>" title="<%=item.title%>" icon="<%=item.icon%>">
		<a href="#">
			<i class="icon <%=item.icon%>"></i><span><%=item.title%></span>
			<span class="pull-right-container">
    			<i class="fa fa-angle-left pull-right"></i>
    		</span>
		</a>
		<%if(item.submenu.length > 0){%>
			<ul class="treeview-menu" >
				<%$.each(item.submenu, function (i,data) {
					it = data.toJSON();	
				%>
					<li id="<%=it.id%>" act="<%=it.url%>" title="<%=it.title%>" icon="<%=it.icon%>" >
						<a href="#"><i class="icon <%=it.icon%>"></i><%=it.title%></a>
					</li>
				<%});%>
			</ul>
		<%}%>
	</li>
<%});%>