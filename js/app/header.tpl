<a href="#" class="logo">
	<!-- mini logo for sidebar mini 50x50 pixels -->
	<span class="logo-mini">朕</span>
	<!-- logo for regular state and mobile devices -->
	<span class="logo-lg">管理模板</span>
</a>
<nav class="navbar navbar-static-top">
	<!-- Sidebar toggle button-->
	<a href="#" class="icon-toggle" data-toggle="offcanvas" role="button" id="collapse">
		<span class="sr-only">Toggle navigation</span>
	</a>
	<div class="navbar-custom-menu">
		<div class="btn-group" style="width: 100%; margin: 10px;">
			<ul class="fc-color-picker" id="color-chooser">
				<li><a class="text-blue" href="#" ><i class="fa fa-square" id="skin-blue"></i></a></li>
				<li><a class="text-yellow" href="#" ><i class="fa fa-square"id="skin-yellow"></i></a></li>
				<li><a class="text-green" href="#" ><i class="fa fa-square"id="skin-green"></i></a></li>
				<li><a class="text-red" href="#" ><i class="fa fa-square"id="skin-red"></i></a></li>
				<li><a class="text-purple" href="#" ><i class="fa fa-square"id="skin-purple"></i></a></li>
				<li><a class="text-black" href="#" ><i class="fa fa-square"id="skin-black"></i></a></li>
			</ul>
		</div>
	</div>
</nav>

