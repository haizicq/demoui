<!-- 模态框（Modal） -->
<div class="modal fade" id="<%=id%>" data-keyboard="false" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><%= title %></h4>
         </div>
         <div class="modal-body">         	
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button>
            <button type="button" class="btn btn-primary">提交更改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->