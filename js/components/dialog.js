define(['lib','text!components/dialog.tpl'], function(LIB,tpl) {
	Dialog = Backbone.Model.extend({
		defaults:{
			id:"DIALOG-"+LIB.randomId(),
			title:"TITLE",
			url:"",
			content:""
		}
	});
	DialogView = Backbone.View.extend({
		el: $("#_temp"),
		template: _.template(tpl),
		initialize: function() {
			this.dialog = new Dialog();
			this.render();
		},
		render: function() {
			//this.template = _.template(tpl);	
			var h = this.template(this.dialog.toJSON());
            this.$el.html(h);
	    },
	    loadDialog:function(title,url){	    	
	    	this.dialog.set({"id":"DIALOG-"+LIB.randomId(),"title":title,"url":url,"content":content});	    	
	    	this.render();
	    	var u = this.dialog.get('url');
	    	var c = this.dialog.get('content');
	    	var id = this.dialog.get('id');
	    	if(u.length > 0){
	    		$('#'+this.dialog.get('id')).modal({remote:this.dialog.get('url')});
	    	} else {
	    		$("#"+id +' .modal-body').html(c);
	    	}	    	
			$('#'+this.dialog.get('id')).modal('show');
	    },
	    openDialog:function(domId){
	    	var title = $("#"+domId).attr("title");
	    	this.dialog.set({"id":"DIALOG-"+LIB.randomId(),"title":title,"url":"","content":content});	    	
	    	this.render();
	    	var id = this.dialog.get('id');
	    	$("#"+id +' .modal-body').html($("#"+domId).html());	    	
			$('#'+this.dialog.get('id')).modal('show');
	    },
	    closeDialog:function(){
	    	$('#'+this.dialog.get('id')).modal('hide')
	    }
	});
});