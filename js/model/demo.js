define(["lib",'components/dialog'], function(LIB,dialog) {
	var MODEL ={
		init:function(){
			console.log("init demo-----------------1");
			this.DialogWin = new DialogView({el:$('#_appDialog')});
			this.list();
			console.log("init demo-----------------2");			
		},
		list:function(){
			_this = this;//LIB.getConst().ROUTE.APPS_GET_ACTION
			//http://bootstrap-table.wenzhixin.net.cn/documentation/
			$("#table3").bootstrapTable({
				url:LIB.getConst().ROUTE.DATA_ACTION,
				pagination: true,
				sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1,                       //初始化加载第一页，默认第一页
                pageSize: 5,                       //每页的记录行数（*）
                pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                uniqueId: "id",
                columns: [
                	{field: "id",title: "序号"},
                	{field: "title",title: "标题"},
                	{field: "film",title: "电影"},
                	{field: "os",title:"操作",
                		formatter:function(value,row,index){  
		                	return ' <a href="#" class="modify" index="'+ index +'" >查看</a>'; 
		                }                		
                	}
                ],
                search:true,
                onClickCell:function(field,value,row){
		        	console.log("click cell " + field + value + row);;
		        	if(field=="id"){
		        		_this.DialogWin.openDialog("demo-myedit");
		        	}else {
		        		_this.DialogWin.openDialog("demo-myedit2");
		        	}
		        },
			});
		}
	};
	return MODEL;
});