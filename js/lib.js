requirejs.config({
	"baseUrl": "js",
	"urlArgs": 'ver=' + Math.round(Math.random() * 99999),
	"paths": {
		"jquery": "libs/jquery-2.0.3.min",
		"underscore": "libs/underscore-min",
		"text": "libs/text",
		"backbone": "libs/backbone-min",
		"const":"const",
		"bootstrap":"libs/bootstrap.min",
		"bootstrap-table":"libs/bootstrap-table.min",
		"bootstrap-table-zh-CN":"libs/bootstrap-table-zh-CN.min",
		"css":"libs/css.min"
	},
	map: {
	  "*": {
	    "css": "libs/css.min"
	  }
	},
	shim: {
		"underscore": {
			exports: "_"
		},
		"bootstrap":{
			deps: ["jquery","css!../bootstrap/css/bootstrap.min.css"]
		},
		"backbone": {
			deps: ["underscore", "jquery"],
			exports: "Backbone"
		},
		"bootstrap-table":{
			deps: ["jquery","css!../bootstrap/css/bootstrap-table.min.css"]
		},
		"bootstrap-table-zh-CN":{
			deps: ["jquery","bootstrap-table"]
		}
	}
});
define(["backbone", "bootstrap","text","underscore",'jquery','bootstrap-table','bootstrap-table-zh-CN','const'], function(backbone,bootstrap,text,underscore,jquery,table,local,c) {
		console.log("init libary...");
		var LIB = {
		getConst:function(){
			return c;		
		},
		init: function() {
			console.log("libary init !");
		},
		random: function() {
			var time = new Date().getTime();
			var randomparam = 1000 * Math.random() + time;
			return randomparam;
		},
		randomId: function() {
			var randomparam = 1000 * Math.random();
			return Math.round(randomparam);
		},
		isUndefined: function(object) {
			if (typeof(object) == "undefined") {
				return true;
			} else {
				return false;
			}
		},
		isNull: function(object) {
			if (!object && typeof(object) != "undefined" && object != 0) {
				return true;
			} else {
				return false;
			}
		},
		isObject: function(object) {
			if (typeof object === "object" && !(object instanceof Array)) {
				var hasProp = false;
				for (var prop in object) {
					hasProp = true;
					break;
				}
				return hasProp;
			} else {
				return false;
			}
		},
		isArray: function(object) {
			if (typeof object === "object" && (object instanceof Array)) {
				return true;
			} else {
				return false;
			}
		}
	};
	console.log("init lib ok [lib.js]");
	LIB.init();
	return LIB;
})