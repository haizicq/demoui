define(["lib", "const",'components/dialog'], function(LIB, CONST,d) {
	var _login_window = "_login_ui";
	var _login_button = "login_btn";
	var MODEL = {
		init: function() {
			if ($("#"+_login_window).length > 0) {
				console.log("has windows ui !");
			} else {
				$(document.body).append("<div id='_login_ui'></div>");
				console.log("add windows ui !");
			}
			this.d = new DialogView({el:$('#'+_login_window)});
			this.base();
			this.verify();
		},
		verify:function(){
			//http://www.oschina.net/question/2563496_2154416
			var _this = this;
			var maxTime = 1*60 * 30; // seconds
			var time = maxTime;
			$('body').on('keydown mousemove mousedown', function(e){
			    time = maxTime; // reset
			    console.log("has move reset time~~~~");
			});
			var intervalId = setInterval(function(){
			    time--;
			    if(time <= 0) {
			    	_this.auth();
			        clearInterval(intervalId);
			    }
			}, 1000);
		},
		login:function(){
			var param = {"random":LIB.random()};
			param = $.extend(param,{"name":$("#loginname").val()});
			param = $.extend(param,{"password":$("#password").val()});
			$.ajax({
				type: "POST",
				url: CONST.ROUTE.LOGIN_ACTION,
				data: JSON.stringify(param), 
				async: false,
				dataType: "json",
				contentType: 'application/json',      
				success: function(data) {
					_menus = data;
					console.log("URI  [" + CONST.ROUTE.LOGIN_ACTION + " ]" + data + "  " + document.location);
					if(LIB.isObject(data) && data.code === CONST.STATUS.SUCCESS){
	
						window.location.replace("main.shtml");
					} else {
						$("#info").html(data.message);
					}
				}
			});
		},
		windowLoad:function(){
			t = this;
			$("#login_btn").click(function(){
				t.login();
			});
		},
		base:function(){
			_this = this;
			$.ajaxSetup({
				//contentType: 'application/json;charset=utf-8', 
				//contentType: "application/x-www-form-urlencoded;charset=utf-8",
				cache: false,
				complete: function(XMLHttpRequest, textStatus) {
					try {
						var data = $.parseJSON(XMLHttpRequest.responseText);
						console.log("ajax filter data:[" + data + "    " + LIB.isObject(data) + "]");
						if(LIB.isObject(data) && data.status === CONST.STATUS.SESSION_TIME_OUT){
							_this.d.loadDialog('请登录','page/login.html');
						}
					} catch(e) {};
					
				}
			});
			console.log("ajax filter init ok! ");
		},
		auth:function(){
			var _this = this;
			$.ajax({
				type: "get",
				url: CONST.ROUTE.AUTH_ACTION,
				async: false,
				dataType: "json",
				success: function(data) {
					_menus = data;
					console.log("URI  [" + CONST.ROUTE.AUTH_ACTION + " ]" + data);
					if(LIB.isObject(data) && data.status === CONST.STATUS.SUCCESS){
						console.log("auth verify ok.");
					} else {
						_this.d.loadDialog('请登录','page/login.html');
					}
				}
			});
		},
		lockScreen:function(){
			this.d.loadDialog('请登录','page/login.html');
		}
	};
	MODEL.init();
	return MODEL;
});